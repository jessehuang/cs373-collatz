#!/usr/bin/env python3

# ----------
# Collatz.py
# ----------

# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from typing import IO, Tuple, Dict

history: Dict[int, int] = {1: 1}
chunks: Dict[int, int] = {}

# ------------
# collatz_read
# ------------


def collatz_read(s: str) -> Tuple[int, int]:
    """
    read two ints
    s a string
    return a tuple of two ints
    """
    a = s.split()
    return int(a[0]), int(a[1])


# ------------
# fill_chunks
# ------------


def fill_chunks(e: Dict[int, int]):
    """
    e a cache of chunks
    fills chunks with max value of 1000 intervals, initializes first cache value
    """
    max_cycle = 0
    for n in range(1, 999001):
        c = cycle_length(n)
        max_cycle = c if max_cycle < c else max_cycle
        if (n % 1000) == 0:
            e[n] = max_cycle
            max_cycle = 0


# ------------
# collatz_eval
# ------------


def collatz_eval(t: Tuple[int, int]) -> Tuple[int, int, int]:
    """
    t a tuple of two ints
    return a tuple of three ints
    """
    i, j = t
    # <your code>
    # precondition check
    assert 0 < i < 1000000
    assert 0 < j < 1000000
    # fills chunks for unit tests
    if len(chunks) == 0:
        fill_chunks(chunks)
    largest_cycle = 0
    end = max([i, j])
    start = max([end // 2 + 1, min([i, j])])
    while start < end + 1:
        # skips a thousand if possible
        if (start - 1) % 1000 == 0 and start + 1000 < end:
            start += 1000
            largest_cycle = (
                chunks[start - 1]
                if chunks[start - 1] > largest_cycle
                else largest_cycle
            )
        else:
            k = cycle_length(start)
            if k > largest_cycle:
                largest_cycle = k
            start += 1
    assert largest_cycle > 0
    return i, j, largest_cycle


# ------------
# cycle_length
# ------------


def cycle_length(n: int) -> int:
    assert n > 0
    temp_hist = []
    c = 0
    while n not in history:
        # store n in to be recorded cache
        temp_hist.append(n)
        if (n % 2) == 0:
            n = n // 2
        else:
            temp_hist.append(n * 3 + 1)
            n = n + (n >> 1) + 1
            c += 1
        c += 1
    # found old cycle length
    c += history[n]
    out = c
    # update cache
    for x in temp_hist:
        history[x] = c
        c -= 1
    assert out > 0
    return out


# -------------
# collatz_print
# -------------


def collatz_print(sout: IO[str], t: Tuple[int, int, int]) -> None:
    """
    print three ints
    sout a writer
    t a tuple of three ints
    """
    i, j, v = t
    sout.write(str(i) + " " + str(j) + " " + str(v) + "\n")


# -------------
# collatz_solve
# -------------


def collatz_solve(sin: IO[str], sout: IO[str]) -> None:
    """
    sin  a reader
    sout a writer
    """
    testcases = 0
    fill_chunks(chunks)
    for s in sin:
        testcases += 1
        assert testcases < 1000  # precondition check
        collatz_print(sout, collatz_eval(collatz_read(s)))
    assert testcases > 0
